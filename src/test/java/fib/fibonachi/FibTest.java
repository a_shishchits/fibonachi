package fib.fibonachi;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by user on 12.09.2017.
 */
public class FibTest extends TestCase {
    public void testF() throws Exception {
        int actual = Fib.f(10);
        int expexted = 55;

        Assert.assertEquals(actual,expexted);
    }

}