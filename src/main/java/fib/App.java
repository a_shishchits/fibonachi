package fib;


import fib.fibonachi.Fib;

import java.util.Scanner;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.println("Enter fibonachi index: ");
        int index = s.nextInt();

        System.out.println("index: " + index + " fibonachi number: " + Fib.f(index));
    }
}
