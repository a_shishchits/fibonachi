package fib.fibonachi;

import java.math.BigInteger;

/**
 * Created by user on 12.09.2017.
 */
public final class Fib {
    public static int f(int index) {
        if (index <= 0) {
            return 0;
        } else if (index == 1) {
            return 1;
        } else if (index == 2) {
            return 1;
        } else {
            return f(index - 1) + f(index - 2);
        }
    }
}
